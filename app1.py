import streamlit as st
import cv2
import numpy as np
import pandas as pd
from descriptors import bitdesc, haralick, glcm
from distances import distance_selection
from upload import upload_file
import time

def main():
    print("App launched!")
    input_value = st.sidebar.number_input("Enter a value", min_value=1, max_value=500, value=10, step=1)
    st.sidebar.write(f"You entered {input_value}")
 
    options = ["Euclidean", "Canberra", "Manhattan", "Chebyshev", "Minkowsky"]
    distance_option = st.sidebar.selectbox("Select a distance", options)
    st.sidebar.write(f"You chose {distance_option}")
    
    descriptor_options = ["Bitdesc", "Haralick", "GLCM"]
    descriptor_choice = st.sidebar.selectbox("Select a descriptor", descriptor_options)
    st.sidebar.write(f"You chose {descriptor_choice}")

    if descriptor_choice == "Bitdesc":
        signatures = np.load('cbir_bitdesc_signatures_v1.npy')      
    elif descriptor_choice == "Haralick":
        signatures = np.load('cbir_haralick_signatures_v1.npy')       
    elif descriptor_choice == "GLCM":
        signatures = np.load('cbir_glcm_signatures_v1.npy')
        
  

    distanceList = list()
    is_image_uploaded = upload_file()
    if is_image_uploaded:
        st.write('''
                 # Search Results
                 ''')
      
        query_image = 'uploaded_images/query_image.png'
     
        img = cv2.imread(query_image, 0)

        if descriptor_choice == "Bitdesc":
            bit_feat = bitdesc(img)
        elif descriptor_choice == "Haralick":
            bit_feat = haralick(img)
        elif descriptor_choice == "GLCM":
            bit_feat = glcm(img)
        

        

        start_time = time.time()  
        for sign in signatures:
            sign = np.array(sign)[0:-2].astype('float')
            sign = sign.tolist()
            distance = distance_selection(distance_option, bit_feat, sign)
            distanceList.append(distance)
        print("Distance computed successfully")
        minDistances = list()
        for i in range(input_value):
            array = np.array(distanceList)
            index_min = np.argmin(array)
            minDistances.append(index_min)
            max = array.max()
            distanceList[index_min] = max
        print(minDistances)
        image_paths = [signatures[small][-1] for small in minDistances]
        classes = [signatures[small][-2] for small in minDistances]
        classes = np.array(classes)
        unique_values, counts = np.unique(classes, return_counts=True)
        list_classes = list()
        print("Unique value with their counts")
        for value, count in zip(unique_values, counts):
            print(f"{value}:{count}")
            list_classes.append(value)
    
        df = pd.DataFrame({"Value": unique_values, "frequency": counts})
        # st.write(df)
       
        st.bar_chart(df.set_index("Value"))

       
       
        st.write("Prediction:", list_classes[0])  
        st.write("Number of Similar Images:", input_value)
        elapsed_time = time.time() - start_time
        st.write("Time Taken (seconds):", elapsed_time)

        
    st.write('''
             # Similar Images
             ''')
    for i, image_path in enumerate(image_paths):
         img = cv2.imread(image_path)
         if img is not None:
            img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            desired_width = 150
            st.image(img_rgb, caption=f"Similar Image {i+1}",width=desired_width)
         else:
            st.write(f"Error loading image: {image_path}")
       
                     


    else:
        st.write("Welcome! Please upload an image to get started ...")

if __name__ == "__main__":
    main()
