import numpy as np
from os import listdir
from descriptors import bitdesc , haralick,glcm
from paths import kth_dir, kth_path
import cv2

def main():
    #extract_with_haralick()
    extract_with_bitdesc()
    extract_with_glcm()


def extract_with_bitdesc():
    # List for all the signatures from inages with BiTdec, etc..
    listOflists = list()
    print('Extracting bitdesc features ....')
    # Loop in path and grab subfolders
    counter = 0
    for kth_class in kth_dir:
        print(f'Current folder: {kth_class}')
        # Grab files from subfolders
        for filename in listdir(kth_path + kth_class+'/'):
            counter += 1
            img_name = f'{kth_path}{kth_class}/{filename}'
            #print(img_name)
            # Read/Load Image as gray
            img = cv2.imread(img_name, 0)
            features = bitdesc(img) + [kth_class] + [img_name]
            print(f' Image count: {counter}')
            # Add image features to listOflists
            listOflists.append(features)
    final_array = np.array(listOflists)
    np.save('cbir_bitdesc_signatures_v1.npy', final_array)
    print('Extraction concluded successfully!') 


def extract_with_haralick():
    listOflists = list()
    print('Extracting haralick features ....')
    # Loop in path and grab subfolders
    counter = 0
    for kth_class in kth_dir:
        print(f'Current folder: {kth_class}')
        # Grab files from subfolders
        for filename in listdir(kth_path + kth_class+'/'):
            counter += 1
            img_name = f'{kth_path}{kth_class}/{filename}'
            #print(img_name)
            # Read/Load Image as gray
            img = cv2.imread(img_name, 0)

            haralick_feat = haralick(img)

            d = [haralick_feat[0], haralick_feat[1], haralick_feat[2], haralick_feat[3],
                    haralick_feat[4], haralick_feat[5], haralick_feat[6], haralick_feat[7],   
                    haralick_feat[8], haralick_feat[9], haralick_feat[10], haralick_feat[11],   
                    haralick_feat[12]]

            features = d + [kth_class] + [img_name]
            print(f' Image count: {counter}')
            # Add image features to listOflists
            listOflists.append(features)
    final_array = np.array(listOflists)
    np.save('cbir_haralick_signatures_v1.npy', final_array)
    print('Extraction concluded successfully!') 



def extract_with_glcm():
    # List for all the signatures from inages with BiTdec, etc..
    listOflists = list()
    print('Extracting bitdesc features ....')
    # Loop in path and grab subfolders
    counter = 0
    for kth_class in kth_dir:
        print(f'Current folder: {kth_class}')
        # Grab files from subfolders
        for filename in listdir(kth_path + kth_class+'/'):
            counter += 1
            img_name = f'{kth_path}{kth_class}/{filename}'
            #print(img_name)
            # Read/Load Image as gray
            img = cv2.imread(img_name, 0)
            features = glcm(img) + [kth_class] + [img_name]
            print(f' Image count: {counter}')
            # Add image features to listOflists
            listOflists.append(features)
    final_array = np.array(listOflists)
    np.save('cbir_glcm_signatures_v1.npy', final_array)
    print('Extraction concluded successfully!') 


if __name__ =="__main__":
    main()