from BiT import bio_taxo
import cv2 , BiT
import mahotas.features as ft
from BiT import biodiversity,taxonomy #Bitdesc
from skimage.feature import graycomatrix,graycoprops
import numpy as np
import skimage.feature as skif

 

def bitdesc(data):
    all_statistics = bio_taxo(data)
    return all_statistics

def haralick(data):
    all_statistics = ft.haralick(data).mean(0)
    return all_statistics



def glcm(data):
    glcm = graycomatrix(data, [2] , [0] , 256 , symmetric=True , normed=True)
    diss = graycoprops(glcm,'dissimilarity')[0,0]
    cont = graycoprops(glcm,'contrast')[0,0]
    corr = graycoprops(glcm,'correlation')[0,0]
    energ = graycoprops(glcm,'energy')[0,0]
    homo = graycoprops(glcm,'homogeneity')[0,0]
    all_statistics = [diss,cont,corr,energ,homo]
    return all_statistics


 

